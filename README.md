# MMM Ont Hybrid Assembly WorKflow (MOHAWK)

## Run

A csv formatted file with sample name, guuid, and nnaopore file location is
required for --inCsv

```
  bash
  nextflow run \
  main.nf \
  -with-singularity \
  /mnt/microbio/HOMES/nick/images/unicyclerdock-2018-04-30-ca6d310fc03f.img \
  -with-trace \
  --inCsv samples.csv \
  --ilines 8000000 \
  -with-dag flowchart.png
```

This can be run using only long read nanopore files using the --longOnly flag. When run in this way the reads are not filtered by default, if you wish for them to be use the --forceFiltLong flag.

```
  bash
  nextflow run \
  main.nf \
  -with-singularity \
  /mnt/microbio/HOMES/nick/images/unicyclerdock-2018-04-30-ca6d310fc03f.img \
  -with-trace \
  --inCsv samples.csv \
  --ilines 8000000 \
  -with-dag flowchart.png
  --longOnly --forceFiltLong
```

## Profiles

Some profiles are implemented through the nextflow.config file to consolidate some common settings. These can be used as follows:

```
nextflow run main.nf -profile csslurm --inCsv NBD_373_CSV.csv --ilines 8000000
```

## output
Three folder will be generated

### Assemblies
Assemblies containing unicycer Assembly.

### Annotation
Annotation containing blast6 formatted Diamond blastp results from prodigal ORF
predicted protein sequences

### remap
Some stats and pngs of coverage from remapped illumina and ONT reads to the assembled
contig (biggest one).
