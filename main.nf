
params.inCsv=file('files.csv')
glist = file(params.inCsv)
Channel
        .from( glist )
        .splitCsv()
        .map { row -> tuple(row[0], row[1], row[2]) }
        .view()
        .into{ ginputs1; ginputs2; ginputs3; ginputs4; ginputs5; ginputs6 }

params.inbams='bams'
params.uniprotDB = 'uniprot/nr.dmnd'
params.ilines=4000000
params.filtbases=500000000
params.longOnly=false
params.forceFiltLong=false
params.notifyEmail=false

if (params.notifyEmail) {
    process sendStartNotification {
        label 'runLocal'
        cache false

        def subj = "MOHAWK - ${workflow.runName} started at ${workflow.start}"

        def msg = """\
Pipeline initialisation summary
-------------------------------
ProjectDir     : ${workflow.projectDir}
WorkDir        : ${workflow.workDir}
ConfigFiles    : ${workflow.configFiles}
Profile        : ${workflow.profile}
Container      : ${workflow.containerEngine} - ${workflow.container}
CommitID       : ${workflow.commitId}
CommandLine    :
${workflow.commandLine}
            """.stripIndent()

        sendMail(to: params.notifyEmail, subject: subj, body: msg)

        script:
        """
        echo "Sending notification email"
        """
    }
}

if (!params.longOnly) {
    process fetchbam {
        tag { sample }
        label 'runLocal'

        input:
        set val(sample),val(guuid),val(nano) from ginputs1

        output:
        set val(sample),file('in.bam') into bamin1

        script:
        """
        echo ${sample} ${guuid} ${nano}
        rsync -avP  -e 'ssh -p 8082' compass@129.67.88.83:${params.inbams}/${guuid}/in.bam ./
        """
    }

    process bedtofastq {
        tag { sample }

        input:
        set val(sample),file('in.bam') from bamin1

        output:
        set val(sample),file('r1.fq'),file('r2.fq') into rawFastqFiles1, rawFastqFiles2 

        script:
        """
        bamToFastq -i in.bam -fq r1.full.fq -fq2 r2.full.fq
        head -n ${params.ilines} r1.full.fq > r1.fq
        head -n ${params.ilines} r2.full.fq > r2.fq
        """
    }
}


if (!params.longOnly) {
    process filtLong {
        tag { sample }

        input:
        set val(sample),val(guuid),val(nano),file('r1.fq'),file('r2.fq') from ginputs3.combine(rawFastqFiles1, by: 0)

        output:
        set val(sample),file('r1.fq'),file('r2.fq'),file('filtlong.fastq.gz') into filtlongOut

        script:
        """
        echo ${sample} ${guuid} ${nano}
        dedupe.sh in=${nano} out=dedupe.fastq
        filtlong -1 r1.fq \
             -2 r2.fq \
             --min_length 1000 \
             --keep_percent 90 \
             --target_bases ${params.filtbases} \
             --trim --split 500  \
             dedupe.fastq \
             | gzip > filtlong.fastq.gz
        """
    }
}
else if (params.longOnly && params.forceFiltLong) {
    process filtLong {
        tag { sample }

        input:
        set val(sample),file('dedupe.fastq') from dedupe

        output:
        set val(sample),file('filtlong.fastq.gz') into filtlongOut

        script:
        """
        filtlong --min_length 1000 \
             --keep_percent 90 \
             --target_bases ${params.filtbases} \
             --trim --split 500  \
             dedupe.fastq \
             | gzip > filtlong.fastq.gz
        """
    }
}

if (!params.longOnly) {
    process unicycler {
        tag { sample }
        label 'moderateCPU'
        label 'highMemory'
        publishDir 'assemblies', pattern: '*MDR_unicycler', mode: 'copy'

        input:
        set val(sample),val(guuid),val(nano),file('r1.fq'),file('r2.fq'),file('filtlong.fastq.gz') from ginputs4.combine(filtlongOut, by:0 )

        output:
        set val(sample), file('*MDR_unicycler') into unicycler_out1,unicycler_out2,unicycler_out3

        shell:
        """
        echo ${sample} ${guuid} ${nano}
        unicycler -1 r1.fq \
              -2 r2.fq \
              -l filtlong.fastq.gz \
              -o MDR_unicycler \
              -t !{task.cpus}
        mv MDR_unicycler ${sample}_MDR_unicycler
        """
    }
}
else if (params.longOnly && params.forceFiltLong) {
    process unicycler {
        tag { sample }
        label 'moderateCPU'
        label 'highMemory'
        publishDir 'assemblies', pattern: '*MDR_unicycler', mode: 'copy'

        input:
        set val(sample),val(guuid),val(nano),file('filtlong.fastq.gz') from ginputs5.combine(filtlongOut, by:0 )

        output:
        set val(sample),file('*MDR_unicycler') into unicycler_out1,unicycler_out2,unicycler_out3

        shell:
        """
        unicycler -l filtlong.fastq.gz \
              -o MDR_unicycler \
              -t !{task.cpus}
        mv MDR_unicycler ${sample}_MDR_unicycler
        """
    }
}
else {
    process unicycler {
        tag { sample }
        label 'moderateCPU'
        label 'highMemory'
        publishDir 'assemblies', pattern: '*MDR_unicycler', mode: 'copy'

        input:
        set val(sample),val(guuid),val(nano),file('dedupe.fastq') from ginputs5.combine(dedupe, by:0)

        output:
        set val(sample),file('*MDR_unicycler') into unicycler_out1,unicycler_out2,unicycler_out3

        shell:
        """
        unicycler -l dedupe.fastq \
               -o MDR_unicycler \
              -t !{task.cpus}
        mv MDR_unicycler ${sample}_MDR_unicycler
        """
    }
}

process prodigal {
    tag { sample }
    label 'moderateMemory'

    input:
    set val(sample),file('*MDR_unicycler') from unicycler_out1

    output:
    set val(sample),file('prodigal') into prodigal_out

    script:
    """
    mkdir prodigal
    prodigal \
    -i *MDR_unicycler/assembly.fasta \
    -q -a prodigal/q.faa \
    -o prodigal/q.genes
    """
}

process diamond {
    tag { sample }
    label 'moderateCPU'
    label 'highMemory'
    publishDir 'annotation', mode: 'copy'

    input:
    set val(sample),file('prodigal') from prodigal_out

    output:
    file("${sample}_annotation")

    script:
    uniprotDB = params.uniprotDB
    """
    diamond blastp \
    --threads ${task.cpus} \
    --max-target-seqs 1 \
    --db $uniprotDB \
    --query prodigal/q.faa \
    --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore salltitles  --out prodigal/q.diamond.txt
    mv prodigal ${sample}_annotation
    """
}

process remapONT {
    tag { sample }
    label 'moderateMemory'

    publishDir "remap/ONT/", mode: 'copy'

    input:
    set val(sample),val(guuid),val(nano),file('*MDR_unicycler') from ginputs6.combine(unicycler_out2, by:0 )

    output:
    set file("*.stats"), file("*png") into remapONT_out

    script:

    """

    minimap2 \
    -ax map-ont *MDR_unicycler/assembly.fasta \
    ${nano} > nano.sam
    samtools view -bS nano.sam -o ${sample}_nano.bam
    samtools sort ${sample}_nano.bam -o ${sample}_nano.sorted.bam
    samtools index ${sample}_nano.sorted.bam
    rm nano.sam ${sample}_nano.bam
    python3 /soft/scripts/plotBam.py ${sample}_nano.sorted.bam > ${sample}.stats
    """
}

if (!params.longOnly) {
    process remapIllumina {
        tag { sample }

        errorStrategy 'ignore'

        publishDir "remap/Illumina/", mode: 'copy'

        input:
        set val(sample),file('*MDR_unicycler'),file('r1.fq'),file('r2.fq') from unicycler_out3.combine(rawFastqFiles2, by:0)

        output:
        set file("*.stats"), file("*png") into remapIllumina_out

        script:

        """

        minimap2 -ax sr *MDR_unicycler/assembly.fasta \
        r1.fq r2.fq > illumina.sam
        samtools view -bS illumina.sam -o ${sample}_illumina.bam
        samtools sort ${sample}_illumina.bam -o ${sample}_illumina.sorted.bam
        samtools index ${sample}_illumina.sorted.bam
        rm illumina.sam ${sample}_illumina.bam

        python3 /soft/scripts/plotBam.py ${sample}_illumina.sorted.bam > ${sample}.stats
        """
    }
}

if (params.notifyEmail) {
    workflow.onError {
        def subj = "MOHAWK - ${workflow.runName} encountered an ERROR!"

        def msg = """\
Pipeline Execution summary
--------------------------
ProjectDir     : ${workflow.projectDir}
WorkDir        : ${workflow.workDir}
ConfigFiles    : ${workflow.configFiles}
Profile        : ${workflow.profile}
Container      : ${workflow.containerEngine} - ${workflow.container}
CommitID       : ${workflow.commitId}
CommandLine    :
${workflow.commandLine}

Error Message  : ${workflow.errorMessage}
Error Report   :
${workflow.errorReport}
            """.stripIndent()

        sendMail(to: params.notifyEmail, subject: subj, body: msg)
    }


    workflow.onComplete {
        def subj = "MOHAWK - ${workflow.runName} finished at ${workflow.complete}"

        def msg = """\
Pipeline Execution summary
--------------------------
ProjectDir     : ${workflow.projectDir}
WorkDir        : ${workflow.workDir}
ConfigFiles    : ${workflow.configFiles}
Profile        : ${workflow.profile}
Container      : ${workflow.containerEngine} - ${workflow.container}
CommitID       : ${workflow.commitId}
CommandLine    :
${workflow.commandLine}

Success        : ${workflow.success}
Exit Status    : ${workflow.exitStatus}
RunTime        : ${workflow.duration}
            """.stripIndent()

        sendMail(to: params.notifyEmail, subject: subj, body: msg)
    }
}
