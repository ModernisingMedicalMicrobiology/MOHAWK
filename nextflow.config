/*
 * MOHAWK config file. Mainly including profiles for clurm and Oxford RESCOMP (SGE) execution.
 */

profiles {
    csslurm {

        singularity {
            enabled = true
            autoMounts = true
        }

        executor.$local.queueSize = 2
        process {
            executor = 'slurm'
            container = '/mnt/slurmscratch/singularityIMG/mohawk-2018-06-21-58be11f6cd23.img'
            errorStrategy = { task.exitStatus in [143,140]  ? 'retry' : 'finish' }
            maxRetries = 3

            memory = { 2.GB + (4.GB * (task.attempt - 1)) }
            withLabel: runLocal { executor = 'local' }
            withLabel: moderateCPU { cpus = 4 }
            withLabel: moderateMemory { memory = { 8.GB + (2.GB * (task.attempt - 1)) } }
            withLabel: highMemory { memory = { 16.GB + (20.GB * (task.attempt - 1)) } }
        }

        params.inbams = '/mnt/microbio/ndm-hicf/ogre/raw_input'
        params.uniprotDB = '/mnt/slurmscratch/DBs/uniprot/nr.dmnd'
        params.notifyEmail = 'example@mail.com'

        trace.enabled = true
        timeline.enabled = true
        report.enabled = true
    }

    RESCOMP {

        singularity {
            enabled = true
            autoMounts = true
            runOptions = '-B /well/bag/'
        }

        executor.$local.queueSize = 2
        process {
            executor = 'SGE'
            container = '/well/bag/singularityIMG/mohawk-2018-06-21-58be11f6cd23.img'
            clusterOptions = '-S /bin/bash -P bag.prjc -cwd -V'
            queue = 'short.qc'
            errorStrategy = { task.exitStatus in [143,140]  ? 'retry' : 'finish' }
            maxRetries = 3

            withLabel: runLocal { executor = 'local' }
            withLabel: moderateCPU { 
                clusterOptions = "-S /bin/bash -P bag.prjc -cwd -V"
                cpus = 4
                penv = 'shmem'
            }
            //memory = { 2.GB + (4.GB * (task.attempt - 1)) }
            withName: filtLong { 
                clusterOptions = { "-S /bin/bash -P bag.prjc -cwd -V" }
                cpus = { 10 + (2 * (task.attempt-1)) }
                penv = 'shmem'
            }
            //withLabel: moderateMemory { memory = { 8.GB + (2.GB * (task.attempt - 1)) } }
            withLabel: moderateMemory { 
                clusterOptions = { "-S /bin/bash -P bag.prjc -cwd -V" }
                cpus = { 4 + (2 * (task.attempt-1)) }
                penv = 'shmem'
            }
            //withLabel: highMemory { memory = { 16.GB + (20.GB * (task.attempt - 1)) } }
            withLabel: highMemory { 
                clusterOptions = { "-S /bin/bash -P bag.prjc -cwd -V" }
                cpus = { 6 + (4 * (task.attempt-1)) }
                penv = 'shmem'
            }
        }

        params.inbams = '/mnt/microbio/ndm-hicf/ogre/raw_input'
        params.uniprotDB = '/well/bag/DBs/uniprot/nr.dmnd'
        params.notifyEmail = 'example@mail.com'

        trace.enabled = true
        timeline.enabled = true
        report.enabled = true
    }
}
